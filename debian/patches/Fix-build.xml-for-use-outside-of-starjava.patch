From: Ole Streicher <olebole@debian.org>
Date: Fri, 17 Feb 2017 10:03:26 +0100
Subject: Fix build.xml for use outside of starjava

This includes the following changes:

 * Rename the `extclasspath` elements to `path`. With ant >= 1.6, there is no
   difference (and before, the difference was not relevant for Debian)

 * Ignore local property files

 * Change the starlink installation dir to the standard Java path /usr/share/java/

 * Prefix the name by `starlink-`

 * Adjust the build and test classpaths for Debian (also for CI tests)

 * Set a class path in the jar manifest

 * Set the source file encoding (cp1252), but not the source version (deprecated)

 * Don't sign the jarfile
---
 build.xml | 85 ++++++++++++++++++++++++++++-----------------------------------
 1 file changed, 38 insertions(+), 47 deletions(-)

diff --git a/build.xml b/build.xml
index d2e0a2b..71dc88b 100644
--- a/build.xml
+++ b/build.xml
@@ -32,12 +32,6 @@
  !-->
 
 <project name="Build file for VO" default="build" basedir=".">
-
-  <!-- If either or both of these files exist then any properties
-   !   contained within them will override those defined here.  -->
-  <property file="${user.home}/.stardev.properties"/>
-  <property file=".properties"/>
-
   <!-- Properties will also be set for all environment variables
    !   (PATH becomes "env.PATH"), generally not a good
    !   idea as names are OS dependent -->
@@ -50,13 +44,13 @@
    !-->
 
   <!-- Directory for the Starlink installation (usually /star/java)-->
-  <property name="star.dir" value="${basedir}/../../"/>
+  <property name="star.dir" value="/usr/share/java"/>
 
   <!-- Directory to install into (install target, usually /star/java)-->
   <property name="star.install" value="${star.dir}"/>
 
   <!-- Directory that contains the Starlink jar tree -->
-  <property name="star.jar.dir" value="${star.dir}/lib"/>
+  <property name="star.jar.dir" value="${star.dir}"/>
 
   <!-- Directory that contains the locally built sources (usually
    !   /star/java/source for full distribution) -->
@@ -82,8 +76,8 @@
    !-->
 
   <!-- Define the package name and current versions -->
-  <property name="Name" value="VO"/>
-  <property name="name" value="vo"/>
+  <property name="Name" value="Starjava VO"/>
+  <property name="name" value="starlink-vo"/>
   <property name="version" value="0.2"/>
 
   <!-- The Java package name -->
@@ -119,6 +113,7 @@
   <!-- Directory containing any third-party jars that should be
    !   distributed (normally these would belong in a proper package)-->
   <property name="src.jars.dir" value="${src.dir}/lib"/>
+  <mkdir dir="${src.jars.dir}"/>
 
   <!-- Directory containing any JNI source code -->
   <property name="src.jni.dir" value="${src.dir}/jni"/>
@@ -201,28 +196,34 @@
    !   1.4 is no longer used the extclasspath type can be replaced by a simple
    !   path.
    !-->
-  <extclasspath id="installed.classpath">
-
-    <pathelement location="${star.jar.dir}/axis/axis.jar"/>
-    <pathelement location="${star.jar.dir}/util/util.jar"/>
-    <pathelement location="${star.jar.dir}/table/table.jar"/>
-    <pathelement location="${star.jar.dir}/votable/votable.jar"/>
+  <path id="installed.classpath">
+    <pathelement location="${star.jar.dir}/starlink-pal.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-registry.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-util.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-table.jar"/>
+    <pathelement location="${star.jar.dir}/starlink-votable.jar"/>
+    <pathelement location="${star.jar.dir}/adql.jar"/>
+    <pathelement location="${star.jar.dir}/json.jar"/>
+    <pathelement location="${star.jar.dir}/commons-io.jar"/>
+  </path>
 
-  </extclasspath>
+  <path id="jar.classpath">
+    <pathelement location="${dist.lib.pkg}/starlink-pal.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-registry.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-util.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-table.jar"/>
+    <pathelement location="${dist.lib.pkg}/starlink-votable.jar"/>
+    <pathelement location="${dist.lib.pkg}/adql.jar"/>
+    <pathelement location="${dist.lib.pkg}/json.jar"/>
+    <pathelement location="${dist.lib.pkg}/commons-io.jar"/>
+  </path>
 
   <!-- Local build system jar files.
    !
    !   Name all the jar files of other packages that we depend on, which have
    !   not been installed (should be same packages as in installed.classpath).
    !-->
-  <extclasspath id="built.jarpath">
-
-    <pathelement location="${star.build.dir}/axis/lib/axis/axis.jar"/>
-    <pathelement location="${star.build.dir}/util/lib/util/util.jar"/>
-    <pathelement location="${star.build.dir}/table/lib/table/table.jar"/>
-    <pathelement location="${star.build.dir}/votable/lib/votable/votable.jar"/>
-
-  </extclasspath>
+  <path id="built.jarpath"/>
 
   <!-- Find all local third party jars files.
    !
@@ -315,6 +316,8 @@
     <pathelement location="${tests.dir}"/>
     <pathelement location="${tests.etc.dir}"/>
     <path refid="classpath"/>
+    <pathelement location="${star.jar.dir}/${name}.jar"/>
+    <pathelement location="${star.jar.dir}/junit.jar"/>
   </path>
 
   <!-- Turn this path into a string which is passed to the tests -->
@@ -344,12 +347,6 @@
   <target name="check_packages"
           unless="runonly.install">
 
-    <!-- AXIS -->
-    <available property="axis.present"
-               classpathref="classpath"
-               classname="org.apache.axis.client.Service"/>
-    <fail message="No AXIS available" unless="axis.present"/>
-
     <!-- UTIL -->
     <available property="util.present"
                classpathref="classpath"
@@ -420,11 +417,13 @@
            destdir="${build.classes}"
            debug="${debug}"
            deprecation="${deprecation}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            optimize="${optimize}">
       <compilerarg value="-Xlint:all,-path,-serial"/>
       <classpath refid="classpath"/>
       <include name="**/*.java"/>
+      <exclude name="uk/ac/starlink/vo/VORegResource.java"/>
     </javac>
 
     <!-- Copy extra files that should live with packages classes
@@ -461,7 +460,10 @@
           description="-> creates the package jar file(s)">
 
     <mkdir dir="${dist.lib.pkg}"/>
-
+    <manifestclasspath property="jar.class.path"
+                       jarfile="${dist.lib.pkg}/${name}.jar">
+      <classpath refid="jar.classpath" />
+    </manifestclasspath>
     <jar destfile="${dist.lib.pkg}/${name}.jar"
          basedir="${build.classes}">
       <manifest>
@@ -470,17 +472,6 @@
       </manifest>
     </jar>
 
-    <!-- Sign all jar files -->
-    <antcall target="signjars"/>
-  </target>
-
-  <target name="signjars" if="sign.jars">
-    <signjar alias="${webstart.alias}"
-             keystore="${webstart.keystore}"
-             keypass="${webstart.keypass}"
-             storepass="${webstart.storepass}">
-      <fileset dir="${dist.lib.pkg}" includes="*.jar"/>
-    </signjar>
   </target>
 
   <!--
@@ -519,7 +510,6 @@
          <!-- Name any extension packages we directly depend on -->
          <extension name="RAYREG" href="rayreg.jnlp"/>
          <extension name="REGISTRY" href="registry.jnlp"/>
-         <extension name="AXIS" href="axis.jnlp"/>
          <extension name="UTIL" href="util.jnlp"/>
          <extension name="TABLE" href="table.jnlp"/>
          <extension name="VOTABLE" href="votable.jnlp"/>
@@ -914,7 +904,7 @@
              windowtitle="${Name} API"
              doctitle="${Name}"
              defaultexcludes="yes"
-             source="${source.version}"
+             encoding="cp1252"
              classpathref="classpath">
       <arg value="-Xdoclint:all,-missing"/>
 
@@ -996,7 +986,8 @@
     <javac srcdir="${tests.dir}"
            destdir="${build.tests}"
            debug="${debug}"
-           source="${source.version}"
+           encoding="cp1252"
+           includeantruntime="false"
            deprecation="${deprecation}" >
       <classpath refid="tests-classpath"/>
 
